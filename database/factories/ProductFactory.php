<?php

use Faker\Generator as Faker;
use App\Product;

$factory->define(Product::class, function (Faker $faker) {
    $products = [
		'1Unit', '2Unit', '3Unit', '4Unit', '5Unit', 'Tower', 'Big Tower', 'Bulbasaur'
	];
	  
	return [
        'art' 	=> strstr($faker->unique()->uuid, '-', true),
		'name' 	=> $faker->unique()->randomElement($products),
    ];
});
