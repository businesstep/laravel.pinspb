<?php

use Faker\Generator as Faker;
use App\Role;

$factory->define(Role::class, function (Faker $faker) {
    $roles = [
		['slug' => 'admin', 'name' => 'Администратор'],
		['slug' => 'manager', 'name' => 'Менеджер']
	];
	
	$index = $faker->unique()->randomElement([0,1]);
	  
	return [
		'slug' => $roles[$index]['slug'], 
		'name' => $roles[$index]['name']
	];
});
