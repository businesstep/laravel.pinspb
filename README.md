# laravel.pinspb

Composer-пакет для Laravel 5.
Функционал: добавление и редактирование продуктов.

## 1. Установка
- Сколонировать репозиторий:
`git clone https://bitbucket.org/businesstep/laravel.pinspb.git`

- Перейти в директорию проекта:
`cd laravel.pinspb`

- В файле *.env* указать доступы к БД

- Установить Composer зависимости, выполнив команду:
`composer install`

- Сгенерировать ключ, выполнив команду:
`php artisan key:generate`

- Установить Node.js зависимости, выполнив команду:
`npm install`

- Запустить локально сервер:
Например, для XAMPP запустить MySQL сервер и запустить PHP командой `php artisan serve`

- Запустить БД миграции, выполнив команду:
`php artisan migrate:fresh`

- Запустить команды seed ля заполнения БД данными:
`php artisan db:seed --class=RolesTableSeeder`
`php artisan db:seed --class=UserTableSeeder`
`php artisan db:seed --class=ProductsTableSeeder`