<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'art'  => 'required|alpha_num',
			'name' => 'required|min:10|max:255'
        ];
    }
	
	/**
	 * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'art.required' 	=> 'Необходимо указать артикул',
			'art.alpha_num' => 'Только латинские символы и цифры',
            'name.required' => 'Необходимо указать название',
			'name.min' 		=> 'Название должно быть не менее 10 символов'
        ];
    }
}
