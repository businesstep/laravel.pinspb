<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['art', 'name'];
	
	public function getCreatedDateAttribute()
	{
		return $this->created_at->format('Y-m-d');
	}
}
