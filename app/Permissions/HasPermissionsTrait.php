<?php
namespace App\Permissions;

use App\Role;
use App\User;

trait HasPermissionsTrait
{
    /**
     * Setup user relations
     *
     * @return App\Role
     */
	public function role()
    {
        return $this->belongsTo('App\Role');
    }
	
    /**
     * Checking roles by slug
     *
     * @param mixed ...$roles
     * @return bool
     */
    public function hasRole(... $roles)
    {
        foreach($roles as $role)
        {
			if($this->role->slug === $role)
            {
                return true;
            }
        }

        return false;
    }
}