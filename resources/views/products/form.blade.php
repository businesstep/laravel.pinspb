@role('admin')								
	<div class="form-group">
		<label for="name">Артикул:</label>
		<input type="text" value="{{ old('art', $product->art) }}" class="form-control {{ $errors->has('art') ? 'is-invalid' : '' }}" name="art" />
		@if ($errors->has('art'))
			<span class="text-danger">{{ $errors->first('art') }}</span>
		@endif
	</div>
@endrole
<div class="form-group">
	<label for="price">Название:</label>
	<input type="text" value="{{ old('name', $product->name) }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" />
	@role('manager')								
		<input type="hidden" value="{{ old('art', $product->art) }}" name="art" />
	@endrole

	@if ($errors->has('name'))
		<span class="text-danger">{{ $errors->first('name') }}</span>
	@endif
</div>
<button type="submit" class="btn btn-primary">{{ $button }}</button>