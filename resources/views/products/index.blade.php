@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
			@if(session()->get('success'))
				<div class="alert alert-success">
					{{ session()->get('success') }}  
				</div>
			@endif
            <div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-10">
							<h1>Список товаров</h1>
						</div>
						<div class="col-md-2">
							@role('admin', 'manager')								
								<a href="{{ route('products.create') }}" class="btn btn-success">Создать товар</a>
							@endrole
						</div>
					</div>
				</div>
				<div class="card-body">
					@foreach ($products as $product)
						<div class="media">
							<div class="media-body">
								<h3 class="mt-0">{{ $product->name }}</h3>
								{{ $product->art }}
								<small class="text-muted">{{ $product->created_date}}</small>
								<br/>
								@role('admin', 'manager')								
									<form action="{{ route('products.destroy', $product->id) }}" method="post">
										<a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Обновить</a>
										@csrf
										@method('DELETE')
										<button class="btn btn-danger" type="submit">Удалить</button>
									</form>
								@endrole
							</div>
						</div>
						<hr>
					@endforeach
					
					{{ $products->links() }}
				</div>
            </div>
        </div>
    </div>
</div>
@endsection