@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="pull-left">
						<h2>Добавление продукта</h2>
					</div>
					<div class="pull-right">
						<a href="{{ route('products.index') }}">Список продуктов</a>
					</div>					
				</div>
				<div class="card-body">
					<form method="post" action="{{ route('products.store') }}">
						@csrf
						@include ('products.form', ['button' => 'Создать продукт']) 
					</form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
