@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
			<div class="row table">
				<div class="col-md-1">№</div>
				<div class="col-md-2">Имя</div>
				<div class="col-md-3">Email</div>
				<div class="col-md-5">Пароль</div>
			</div>
			@foreach($users as $user)
				<div class="row table">
					<div class="col-md-1">{!! $user->id !!}</div>
					<div class="col-md-2">{!! $user->name !!}</div>
					<div class="col-md-3">{!! $user->email !!}</div>
					<div class="col-md-5">123456</div>
				</div>
			@endforeach
        </div>
    </div>
	<div class="row justify-content-center">
        <div class="col-md-12">
			<p>Сomposer-пакет для Laravel 5, содержащий и реализующий нижеследующее Функционал: добавление и редактирование продуктов.</p>
		</div>
    </div>
</div>
@endsection
